const editor = require('editor');
const fs = require('fs');
const path = require('path');
const { exec, execSync } = require('child_process');

const mapFiles = require('./mapFiles');
const input = require('./input');
const paths = require('./paths');
const todos = require('./todos');
const { encrypt, decrypt } = require('./secret');

const ENC = 'utf-8';
const TMP_FILE = '/tmp/.chillist.md';

function exists(file) {
  try {
    return fs.existsSync(file);
  } catch (err) {
    return false;
  }
}

function sync(command) {
  execSync(command, { cwd: paths.STORE });
}

// Initialize the store
if (!exists(paths.STORE)) {
  fs.mkdirSync(paths.STORE);
}

const gitDirectory = path.join(paths.STORE, '.git');
if (!exists(gitDirectory)) {
  sync('git init');
  sync('git commit --allow-empty -m "The chillist"');
}

// Initialize the todos
if (!exists(paths.TODOS)) {
  fs.writeFileSync(paths.TODOS, '{}', ENC);
  sync(`git add -A && git commit -am 'Empty todos db'`);
}

if (!exists(paths.GITIGNORE)) {
  fs.writeFileSync(paths.GITIGNORE, 'tmp', ENC);
  sync(`git add -A && git commit -am 'Ignoring tmp directory'`);
}

const requiredDirectories = [
  paths.CALENDAR,
  paths.NOTES,
  paths.PROJECTS,
  paths.TMP,
];

requiredDirectories.forEach((dir) => {
  if (!exists(dir)) {
    fs.mkdirSync(dir);
  }
});

// Attempt to require todos database
let todosDatabase = {};
try {
  // eslint-disable-next-line global-require, import/no-dynamic-require
  todosDatabase = require(paths.TODOS);
  // eslint-disable-next-line no-empty
} catch (err) {}

function shell(command, cb) {
  exec(command, { cwd: paths.STORE }, (err, stderr, stdout) => {
    if (cb) {
      cb(err, stderr, stdout);
      return;
    }

    if (err) {
      process.stderr.write(err.stack || err.message);
      process.exit(1);
    }

    if (stderr) process.stderr.write(stderr);
    if (stdout) process.stdout.write(stdout);
  });
}

function getBasename(filePath) {
  return filePath.replace(paths.STORE, '').replace(path.extname(filePath), '');
}

function createIfNotExists(filePath) {
  if (!exists(filePath)) {
    fs.writeFileSync(filePath, '', ENC);
  }
}

function updateTodos(filePath, text) {
  const key = getBasename(filePath);
  const list = todos(text);
  if (list.length) {
    todosDatabase[key] = list;
  } else {
    delete todosDatabase[key];
  }
}

function parseAndSaveTodos(filePath, text) {
  updateTodos(filePath, text);
  fs.writeFileSync(paths.TODOS, JSON.stringify(todosDatabase, null, 2), ENC);
}

function commit(msg) {
  shell('git status --porcelain', (err, stdout) => {
    if (err) {
      process.stderr.write(err);
      process.exit(1);
    }
    const untrackedChanges = stdout !== '';

    if (untrackedChanges) {
      shell(`git add -A && git commit -am '${msg}'`);
    } else {
      shell(`git add -A && git reset --hard HEAD`, () => {
        process.exit(1);
      });
    }
  });
}

function verifyAndCommit(filePath) {
  const basename = getBasename(filePath);
  const text = fs.readFileSync(filePath, ENC);
  parseAndSaveTodos(filePath, text);

  if (text) {
    commit(`[edit] ${basename}`, filePath);
  } else {
    fs.unlinkSync(filePath);
    commit(`[rm] ${basename}`, filePath);
  }
}

async function encrypted(filePath = paths.CONTEXT_HASH) {
  const password = await input();

  let decrypted = '';
  const fileExists = fs.existsSync(filePath);
  if (fileExists) {
    const ciphertext = fs.readFileSync(filePath, ENC);
    decrypted = await decrypt(password, ciphertext);
  }
  fs.writeFileSync(TMP_FILE, decrypted, ENC);

  editor(TMP_FILE, async () => {
    const plaintext = fs.readFileSync(TMP_FILE, ENC);
    const commitName = getBasename(filePath);

    fs.unlinkSync(TMP_FILE);

    if (plaintext && plaintext !== decrypted) {
      const encryptedData = await encrypt(password, plaintext);
      fs.writeFileSync(filePath, encryptedData, ENC);
    }

    if (fileExists) {
      if (!plaintext) {
        fs.unlinkSync(filePath);
        commit(`[rm] ${commitName}`, filePath);
      } else {
        commit(`[edit] ${commitName}`, filePath);
      }
    }
  });
}

const book = {
  edit(filePath = paths.CONTEXT_HASH) {
    createIfNotExists(filePath);
    editor(filePath, () => {
      verifyAndCommit(filePath);
    });
  },

  getTodos() {
    return todosDatabase;
  },

  secret(filePath) {
    encrypted(filePath).catch((err) => {
      process.stderr.write(err.message);
      process.exit(1);
    });
  },

  async updateTodosDb() {
    todosDatabase = {};
    await mapFiles((file) => {
      const text = fs.readFileSync(file, ENC);
      updateTodos(file, text);
    });
    fs.writeFileSync(paths.TODOS, JSON.stringify(todosDatabase, null, 2), ENC);
    commit('[sync] todos');
  },
};

module.exports = book;
