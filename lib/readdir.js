const fs = require('fs');

async function readdir(filePath) {
  return new Promise((resolve) => {
    fs.readdir(filePath, (err, value) => {
      if (err) {
        process.stderr.write(err.stack);
        process.exit(1);
      }

      resolve(value);
    });
  });
}

module.exports = readdir;
