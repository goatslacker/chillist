const read = require('read');

async function input() {
  return new Promise((resolve, reject) => {
    read(
      {
        prompt: 'Password:',
        silent: true,
      },
      (err, password) => {
        if (err) {
          reject(err);
        } else {
          resolve(password);
        }
      }
    );
  });
}

module.exports = input;
