const path = require('path');

const paths = require('./paths');
const readdir = require('./readdir');

function map(dirname, arr, mapFunction) {
  return arr.map((file) => mapFunction(path.join(dirname, file)));
}

async function mapCalendar(mapFunction) {
  const calendar = await readdir(paths.CALENDAR);
  return map(paths.CALENDAR, calendar, mapFunction);
}

async function mapNotes(mapFunction) {
  const notes = await readdir(paths.NOTES);
  return map(paths.NOTES, notes, mapFunction);
}

async function mapProjects(mapFunction) {
  const projects = await readdir(paths.PROJECTS);
  return map(paths.PROJECTS, projects, mapFunction);
}

async function mapTemporary(mapFunction) {
  const tmp = await readdir(paths.TMP);
  return map(paths.TMP, tmp, mapFunction);
}

async function mapFiles(mapFunction) {
  const calendar = await mapCalendar(mapFunction);
  const notes = await mapNotes(mapFunction);
  const projects = await mapProjects(mapFunction);
  const tmp = await mapTemporary(mapFunction);

  return [].concat(calendar, notes, projects, tmp);
}

mapFiles.f = {
  mapCalendar,
  mapNotes,
  mapProjects,
  mapTemporary,
};

module.exports = mapFiles;
