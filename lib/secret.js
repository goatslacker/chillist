const openpgp = require('openpgp');

async function encrypt(passwords, text) {
  const options = {
    armor: true,
    message: openpgp.message.fromText(text),
    passwords,
  };

  const ciphertext = await openpgp.encrypt(options);
  return ciphertext.data;
}

async function decrypt(passwords, encrypted) {
  const options = {
    format: 'utf8',
    message: await openpgp.message.readArmored(encrypted),
    passwords,
  };
  const plaintext = await openpgp.decrypt(options);
  return plaintext.data;
}

module.exports = {
  encrypt,
  decrypt,
};
