const path = require('path');

const hash = require('./hash');

const ROOT = path.resolve(path.join(process.env.HOME, '.chillist'));
const CONFIG = path.join(ROOT, '.config.json');

// eslint-disable-next-line import/no-dynamic-require
const config = require(CONFIG);
const STORE = config.root.replace(/^~/, process.env.HOME);

const CALENDAR = path.join(STORE, 'Calendar');
const GITIGNORE = path.join(STORE, '.gitignore');
const NOTES = path.join(STORE, 'Notes');
const PROJECTS = path.join(STORE, 'Projects');
const TMP = path.join(STORE, 'tmp');
const TODOS = path.join(STORE, '.todos.json');

const CONTEXT_HASH = path.join(PROJECTS, `${hash(process.env.PWD)}.md`);

module.exports = {
  CALENDAR,
  CONFIG,
  CONTEXT_HASH,
  GITIGNORE,
  NOTES,
  PROJECTS,
  STORE,
  TMP,
  TODOS,
};
