const isTodo = /^ *[-*+] *?\[[x >-]?\]/;
const isDone = /\[x\]/;
const isFwd = /\[>\]/;
const isSkip = /\[-\]/;

function parseTodos(text) {
  return text
    .split('\n')
    .filter((x) => isTodo.test(x))
    .map((x) => ({
      d: isSkip.test(x) ? 3 : isFwd.test(x) ? 2 : isDone.test(x) ? 1 : 0,
      t: x.replace(isTodo, '').trim(),
    }));
}

module.exports = parseTodos;
