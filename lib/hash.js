const crypto = require('crypto');
const path = require('path');

function hash(file) {
  const key = crypto
    .createHash('sha256')
    .update(file, 'utf-8')
    .digest('hex')
    .slice(0, 7);

  return `${key}-${path.basename(file)}`;
}

module.exports = hash;
