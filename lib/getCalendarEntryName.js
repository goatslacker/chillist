const chrono = require('chrono-node');

function getCalendarEntryName(arg) {
  const str = String(arg);
  const date = chrono.parseDate(str);
  if (!date) {
    throw new Error(`Did not understand date: "${str}"`);
  }
  const y = date.getYear() + 1900;
  let m = date.getMonth() + 1;
  m = m < 10 ? `0${m}` : m;
  let d = date.getDate();
  d = d < 10 ? `0${d}` : d;

  return `${y}${m}${d}`;
}

module.exports = getCalendarEntryName;
