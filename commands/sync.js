const { updateTodosDb } = require('../lib/book');

async function sync() {
  await updateTodosDb();
}

module.exports = sync;
