const chalk = require('chalk');
const path = require('path');

const getCalendarEntryName = require('../lib/getCalendarEntryName');
const { getTodos } = require('../lib/book');

const isCalendar = /^\/Calendar\//;

function filter(argv) {
  const today = Number(getCalendarEntryName('today'));

  return (key) => {
    if (argv.overdue) {
      if (!isCalendar.test(key)) {
        return false;
      }

      const basename = path.basename(key, path.extname(key));
      return Number(basename) < today;
    }

    return true;
  };
}

function getStatus(status) {
  return status === 3
    ? chalk.magenta('-')
    : status === 2
      ? chalk.yellow('>')
      : status === 1
        ? chalk.red('x')
        : '';
}

async function backlog(argv) {
  const todos = getTodos();

  let exitCode = 0;

  Object.keys(todos)
    .filter(filter(argv))
    .forEach((key) => {
      const todoList = todos[key].filter((todo) => {
        if (argv.overdue) {
          return todo.d === 0;
        }

        if (typeof argv.status === 'number') {
          return todo.d === argv.status;
        }

        return true;
      });
      if (!todoList.length) return;

      console.log(chalk.green(key));
      todoList.forEach((todo) => {
        let match = false;
        if (argv.search) {
          match = todo.t.indexOf(argv.search) >= 0;
          if (match === false) {
            return;
          }
        }
        const todoText = match
          ? todo.t.replace(argv.search, chalk.yellow(argv.search))
          : todo.t;

        console.log(chalk.gray(`  * [${getStatus(todo.d)}]`), todoText);
        exitCode = 1;
      });
      console.log(chalk.gray('--'));
    });

  process.exit(exitCode);
}

module.exports = backlog;
