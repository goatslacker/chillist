const fs = require('fs');
const { execSync } = require('child_process');

const pkg = require('../package.json');
const readdir = require('../lib/readdir');
const { CONFIG, STORE, TODOS } = require('../lib/paths');

const upgrades = new Map();

upgrades.set(6, async () => {
  const store = await readdir(STORE);
  const isMarkdown = /.md$/;
  const files = store.filter((file) => isMarkdown.test(file));

  if (files.length === 0) {
    console.log('! It seems you have already upgraded from 5->6');
    return;
  }

  files.forEach((file) => {
    console.log('* Moving', file);
    execSync(`git mv ${file} Projects/`, { cwd: STORE });
  });

  const todos = JSON.parse(fs.readFileSync(TODOS, 'utf-8'));
  Object.keys(todos).forEach((key) => {
    const nextKey = `/Projects/${key}`;
    todos[nextKey] = todos[key];
    delete todos[key];
  });
  fs.writeFileSync(TODOS, JSON.stringify(todos, null, 2), 'utf-8');
  execSync('git add .todos.json', { cwd: STORE });

  fs.writeFileSync(CONFIG, JSON.stringify({ root: STORE }, null, 2), 'utf-8');

  execSync('git commit -m "Upgraded to chillist v6"', { cwd: STORE });
});

async function upgrade(argv) {
  const fromVersion = Number(argv.fromMajorVersion);
  const toVersion = Number(pkg.version.split('.')[0]);

  if (fromVersion >= toVersion) {
    throw new Error(`Cannot downgrade from v${fromVersion} to v${toVersion}`);
  }

  // +1 so we can define the upgrade process from 5->6 labeled as 6
  for (let v = fromVersion + 1; v <= toVersion; v += 1) {
    if (upgrades.has(v)) {
      console.log('- Running upgrade', v);
      const time = process.hrtime();
      // eslint-disable-next-line no-await-in-loop
      await upgrades.get(v)();
      const diff = process.hrtime(time);
      console.log(
        '+ Success',
        v,
        `in ${(diff[0] * 1e9 + diff[1]) / 1000000}ms`
      );
    }
  }

  console.log('Done');
  console.log('Version', pkg.version);
}

module.exports = upgrade;
