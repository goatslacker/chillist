const yargs = require('yargs');
const { edit, secret } = require('../lib/book');
const { CONTEXT_HASH, PROJECTS, TMP } = require('../lib/paths');

function cwd(argv) {
  if (argv._.length > 0 && argv._[0] !== 'cwd') {
    yargs.showHelp();
    return;
  }

  const filePath = argv.local
    ? CONTEXT_HASH.replace(PROJECTS, TMP)
    : CONTEXT_HASH;

  if (argv.private) {
    secret(filePath);
  } else {
    edit(filePath);
  }
}

module.exports = cwd;
