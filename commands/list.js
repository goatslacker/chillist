const chalk = require('chalk');
const fs = require('fs');
const { PassThrough } = require('stream');
const { spawn } = require('child_process');

const mapFiles = require('../lib/mapFiles');
const { STORE } = require('../lib/paths');

async function snippet(filePath, long) {
  const fd = fs.openSync(filePath, 'r');
  const len = long ? 240 : 80;
  const buffer = Buffer.alloc(len);

  return new Promise((resolve, reject) => {
    fs.read(fd, buffer, 0, len, 0, (err, num) => {
      if (err) {
        reject(err);
        return;
      }

      const snippetTxt = [
        chalk.green(filePath.replace(STORE, '')),
        buffer
          .toString('utf8', 0, num)
          .replace(/\n/g, '  ')
          .replace(/\W/g, (x) => chalk.gray(x)),
      ].join('\n');

      resolve(snippetTxt);
    });
  });
}

const CALENDAR = /\/Calendar\//;
const NOTES = /\/Notes\//;
const PROJECTS = /\/Projects\//;

function createStream() {
  const stream = new PassThrough();
  process.stdin.pipe(stream);

  const less = spawn('less', ['-r'], {
    stdio: ['pipe', 'inherit', 'inherit'],
  });
  stream.pipe(less.stdin);

  less.stdin.on('error', (err) => {
    if (err.code == 'EPIPE') {
      process.exit(0);
    }
  });

  return stream;
}

async function list(argv) {
  const stream = createStream();

  const files = await mapFiles((filePath) => {
    let time = Infinity;

    if (argv.type) {
      if (argv.type === 'calendar' && !CALENDAR.test(filePath)) {
        return null;
      }
      if (argv.type === 'notes' && !NOTES.test(filePath)) {
        return null;
      }
      if (argv.type === 'projects' && !PROJECTS.test(filePath)) {
        return null;
      }
    }

    try {
      time = fs.statSync(filePath).mtime.getTime();
      // eslint-disable-next-line no-empty
    } catch (err) {}

    return { filePath, time };
  });
  const rest = files.filter(Boolean);
  rest.sort((a, b) => (a.time > b.time ? -1 : 1));

  if (argv.short) {
    rest.forEach((x) => stream.write(x.filePath.replace(/.md$/, '') + '\n'));
  } else {
    for (x of rest) {
      const text = await snippet(x.filePath, argv.long);
      stream.write(`${text}\n`);
    }
    stream.end();
  }
}

module.exports = list;
