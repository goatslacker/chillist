const path = require('path');

const getCalendarEntryName = require('../lib/getCalendarEntryName');
const { CALENDAR, TMP } = require('../lib/paths');
const { edit, secret } = require('../lib/book');

function calendar(argv) {
  const dirname = argv.local ? TMP : CALENDAR;
  const basename = getCalendarEntryName(argv.dateQueryString);
  const filePath = path.join(dirname, `${basename}.txt`);

  if (argv.private) {
    secret(filePath);
  } else {
    edit(filePath);
  }
}

module.exports = calendar;
