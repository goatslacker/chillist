const score = require('string-score');

const mapFiles = require('../lib/mapFiles');
const { STORE } = require('../lib/paths');
const { edit, secret } = require('../lib/book');

async function open(argv) {
  const query = argv.name;

  const files = await mapFiles((filePath) => {
    const basename = filePath.replace(STORE, '');
    return {
      filePath,
      score: score(basename, query),
    };
  });
  const scored = files.filter((file) => file.score > 0);
  scored.sort((a, b) => (a.score > b.score ? -1 : 1));

  const [file] = scored;

  if (file) {
    const { filePath } = file;

    if (argv.private) {
      secret(filePath);
    } else {
      edit(filePath);
    }
  } else {
    process.stderr.write('No file found\n');
    process.exit(1);
  }
}

module.exports = open;
