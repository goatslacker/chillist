const path = require('path');

const { edit, secret } = require('../lib/book');
const { NOTES, TMP } = require('../lib/paths');

function getDateString(timestamp) {
  return new Date(timestamp).toISOString();
}

function replace(str) {
  return str.replace(/\W/g, '-');
}

function note(argv) {
  const dirname = argv.local ? TMP : NOTES;
  const basename = replace(argv.noteName || getDateString(Date.now()));
  const filePath = path.join(dirname, `${basename}.md`);

  if (argv.private) {
    secret(filePath);
  } else {
    edit(filePath);
  }
}

module.exports = note;
